const express = require('express')
const bodyParser = require('body-parser')

const app = express()

const PORT = 3002

app.use(bodyParser.urlencoded({extended: false}))

app.get('/', (req, res) => {
    res.send(`<h1>me chat</h1>`);
});
const serverInit = () => {
    return app.listen(PORT, () =>
        console.log(`server listening at http://localhost:${PORT}`)
    )
}

serverInit()
